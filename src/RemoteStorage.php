<?php

namespace Drupal\entity_remote;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\DatabaseBackendFactory;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\entity_generic\GenericStorage;

/**
 * Defines the storage handler class for entities.
 *
 * This extends the base storage class, adding required special handling for entities.
 */
class RemoteStorage extends GenericStorage {

  /**
   * Constructs a SqlContentEntityStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to be used.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface|null $memory_cache
   *   The memory cache backend to be used.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, Connection $database, EntityFieldManagerInterface $entity_field_manager, CacheBackendInterface $cache, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache = NULL, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, EntityTypeManagerInterface $entity_type_manager = NULL) {
    $additional = $entity_type->get('additional');
    if (isset($additional['entity_remote']['storage'])) {
      $database = Database::getConnection('default', $additional['entity_remote']['storage']);

      $checksum_provider = \Drupal::service('cache_tags.invalidator.checksum');
      $cache_factory = new DatabaseBackendFactory($database, $checksum_provider);
      $cache = $cache_factory->get('entity');
    }

    parent::__construct($entity_type, $database, $entity_field_manager, $cache, $language_manager, $memory_cache, $entity_type_bundle_info, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  protected function getQueryServiceName() {
    return 'entity_remote.entity.query.sql';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery($conjunction = 'AND') {
    $query = parent::getQuery($conjunction);

    // Add entity bundle condition.
    if ($bundle_entity_type = $this->entityType->getBundleEntityType()) {
      if ($bundle_entity_type != $this->entityTypeId) {
        $storage = $this->entityTypeManager->getStorage($bundle_entity_type);
        $bundles = array_keys($storage->loadMultiple());
        $query->condition($this->entityType->getKey('bundle'), $bundles, 'IN');
      }
    }

    return $query;
  }

}
