<?php

namespace Drupal\entity_remote\EntityQuery;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\Query\Sql\QueryFactory as BaseQueryFactory;

/**
 * Entity query implementation.
 */
class QueryFactory extends BaseQueryFactory {

  /**
   * Constructs a QueryFactory object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection used by the entity query.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
    $this->namespaces = QueryBase::getNamespaces($this);
  }

  /**
   * {@inheritdoc}
   */
  public function get(EntityTypeInterface $entity_type, $conjunction) {
    $class = QueryBase::getClass($this->namespaces, 'Query');
    $additional = $entity_type->get('additional');
    if (isset($additional['entity_remote']['storage'])) {
      $database = Database::getConnection('default', $additional['entity_remote']['storage']);
      $this->connection = $database;
    }

    return new $class($entity_type, $conjunction, $this->connection, $this->namespaces);
  }

  /**
   * {@inheritdoc}
   */
  public function getAggregate(EntityTypeInterface $entity_type, $conjunction) {
    $class = QueryBase::getClass($this->namespaces, 'QueryAggregate');
    $additional = $entity_type->get('additional');
    if (isset($additional['entity_remote']['storage'])) {
      $database = Database::getConnection('default', $additional['entity_remote']['storage']);
      $this->connection = $database;
    }

    return new $class($entity_type, $conjunction, $this->connection, $this->namespaces);
  }

}
